﻿using Chapter12;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FrmMovie
{
    [Serializable]
   public  class Cinema
    {
        public Cinema() {
            this.Seats =new Dictionary<string,Seat> ();
            this.SoldTickes = new List<Ticket>();

        }
        public Dictionary<String, Seat> Seats { get; set; }
        public Dictionary<String, Session> Sessions { get; set; }
        public List<Ticket> SoldTickes { get; set; }
        public void SaveTickes() {
            //二进制集合序列化

            FileStream fs = new FileStream(@"‪tickes.txt", FileMode.Create);
            // FileStream fs = new FileStream(@"F:\qyl\FrmChapter12\tickes.txt", FileMode.Create);
           
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, SoldTickes);
            fs.Close();

        }
        public void LoadTickes() {
            //二进制集合反序列化
            try
            {

                FileStream fs = new FileStream(@"‪tickes.txt", FileMode.OpenOrCreate);

                // FileStream fs = new FileStream( @"F:\qyl\FrmChapter12\tickes.txt", FileMode.Open);
                BinaryFormatter bf = new BinaryFormatter();
                SoldTickes = (List<Ticket>)bf.Deserialize(fs);
                fs.Close();
            }
            catch (Exception)
            {
            }
           
          
           

        }
        public void LoadSessions() {
            if (Sessions==null)
            {
                Sessions = new Dictionary<string, Session>();
                Sessions.Clear();
                XmlDocument doc = new XmlDocument();
                doc.Load("movies.xml");
                XmlNode root = doc.DocumentElement;
                foreach (XmlNode mvNode in root.ChildNodes)
                {
                                Movie movie = new Movie();
                    movie.Actors = mvNode["Actor"].InnerText;
                    movie.Company = mvNode["Company"].InnerText;
                    movie.MovieName = mvNode["Name"].InnerText;
                    movie.MovieType =(MovieType)Enum.Parse(typeof(MovieType),mvNode["Type"].InnerText);
                    movie.Picture = mvNode["Picture"].InnerText;
                    movie.Price =int.Parse( mvNode["Price"].InnerText);
                    foreach (XmlNode seNode in mvNode["Session"].ChildNodes)
                    {
                        Session session = new Session();
                        session.Time = seNode.InnerText;
                        session.Movie = movie;
                        Sessions.Add(session.Time,session);
                    }
                }
            }
        }
    }
}
