﻿namespace FrmMovie
{
    partial class FrmMovie
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMovie));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.tvMovie = new System.Windows.Forms.TreeView();
            this.pInfo = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.DisCount = new System.Windows.Forms.ComboBox();
            this.lblDiscount = new System.Windows.Forms.Label();
            this.rbseal = new System.Windows.Forms.RadioButton();
            this.rbVip = new System.Windows.Forms.RadioButton();
            this.rbCom = new System.Windows.Forms.RadioButton();
            this.lblCprice = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.lblStar = new System.Windows.Forms.Label();
            this.lblComplay = new System.Windows.Forms.Label();
            this.pcMovieImg = new System.Windows.Forms.PictureBox();
            this.lblMovieName = new System.Windows.Forms.Label();
            this.pleft = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pseat = new System.Windows.Forms.Panel();
            this.pInfo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcMovieImg)).BeginInit();
            this.pleft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "遇见.jpeg");
            this.imageList1.Images.SetKeyName(1, "女孩.jpg");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("华文中宋", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(70, 137);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "遇见";
            // 
            // tvMovie
            // 
            this.tvMovie.BackColor = System.Drawing.Color.White;
            this.tvMovie.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tvMovie.Location = new System.Drawing.Point(2, 175);
            this.tvMovie.Margin = new System.Windows.Forms.Padding(2);
            this.tvMovie.Name = "tvMovie";
            this.tvMovie.Size = new System.Drawing.Size(201, 268);
            this.tvMovie.TabIndex = 2;
            this.tvMovie.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvMovie_AfterSelect);
            // 
            // pInfo
            // 
            this.pInfo.BackColor = System.Drawing.Color.Transparent;
            this.pInfo.Controls.Add(this.label2);
            this.pInfo.Controls.Add(this.label3);
            this.pInfo.Controls.Add(this.label4);
            this.pInfo.Controls.Add(this.label5);
            this.pInfo.Controls.Add(this.label6);
            this.pInfo.Controls.Add(this.label7);
            this.pInfo.Controls.Add(this.groupBox1);
            this.pInfo.Controls.Add(this.lblCprice);
            this.pInfo.Controls.Add(this.lblPrice);
            this.pInfo.Controls.Add(this.lblTime);
            this.pInfo.Controls.Add(this.lblType);
            this.pInfo.Controls.Add(this.lblStar);
            this.pInfo.Controls.Add(this.lblComplay);
            this.pInfo.Controls.Add(this.pcMovieImg);
            this.pInfo.Controls.Add(this.lblMovieName);
            this.pInfo.Location = new System.Drawing.Point(232, 12);
            this.pInfo.Margin = new System.Windows.Forms.Padding(2);
            this.pInfo.Name = "pInfo";
            this.pInfo.Size = new System.Drawing.Size(589, 177);
            this.pInfo.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(128, 136);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 20);
            this.label2.TabIndex = 14;
            this.label2.Text = "当前票价：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(141, 114);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 20);
            this.label3.TabIndex = 13;
            this.label3.Text = "原票价：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(128, 95);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 20);
            this.label4.TabIndex = 12;
            this.label4.Text = "放映时间：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(155, 73);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "类型：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(155, 53);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 20);
            this.label6.TabIndex = 10;
            this.label6.Text = "主演：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(127, 34);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 20);
            this.label7.TabIndex = 9;
            this.label7.Text = "发行公司：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.DisCount);
            this.groupBox1.Controls.Add(this.lblDiscount);
            this.groupBox1.Controls.Add(this.rbseal);
            this.groupBox1.Controls.Add(this.rbVip);
            this.groupBox1.Controls.Add(this.rbCom);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(369, 98);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(219, 78);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "电影票类型";
            // 
            // DisCount
            // 
            this.DisCount.FormattingEnabled = true;
            this.DisCount.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.DisCount.Location = new System.Drawing.Point(79, 50);
            this.DisCount.Name = "DisCount";
            this.DisCount.Size = new System.Drawing.Size(121, 25);
            this.DisCount.TabIndex = 12;
            this.DisCount.Visible = false;
            this.DisCount.TextChanged += new System.EventHandler(this.DisCount_TextChanged);
            // 
            // lblDiscount
            // 
            this.lblDiscount.AutoSize = true;
            this.lblDiscount.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblDiscount.Location = new System.Drawing.Point(23, 51);
            this.lblDiscount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDiscount.Name = "lblDiscount";
            this.lblDiscount.Size = new System.Drawing.Size(51, 20);
            this.lblDiscount.TabIndex = 11;
            this.lblDiscount.Text = "折扣：";
            this.lblDiscount.Visible = false;
            // 
            // rbseal
            // 
            this.rbseal.AutoSize = true;
            this.rbseal.Location = new System.Drawing.Point(140, 23);
            this.rbseal.Margin = new System.Windows.Forms.Padding(2);
            this.rbseal.Name = "rbseal";
            this.rbseal.Size = new System.Drawing.Size(62, 21);
            this.rbseal.TabIndex = 2;
            this.rbseal.Text = "打折票";
            this.rbseal.UseVisualStyleBackColor = true;
            this.rbseal.CheckedChanged += new System.EventHandler(this.rbseal_CheckedChanged);
            // 
            // rbVip
            // 
            this.rbVip.AutoSize = true;
            this.rbVip.Location = new System.Drawing.Point(79, 23);
            this.rbVip.Margin = new System.Windows.Forms.Padding(2);
            this.rbVip.Name = "rbVip";
            this.rbVip.Size = new System.Drawing.Size(57, 21);
            this.rbVip.TabIndex = 1;
            this.rbVip.Text = "VIP票";
            this.rbVip.UseVisualStyleBackColor = true;
            this.rbVip.CheckedChanged += new System.EventHandler(this.rbVip_CheckedChanged);
            // 
            // rbCom
            // 
            this.rbCom.AutoSize = true;
            this.rbCom.Checked = true;
            this.rbCom.Location = new System.Drawing.Point(13, 23);
            this.rbCom.Margin = new System.Windows.Forms.Padding(2);
            this.rbCom.Name = "rbCom";
            this.rbCom.Size = new System.Drawing.Size(62, 21);
            this.rbCom.TabIndex = 0;
            this.rbCom.TabStop = true;
            this.rbCom.Text = "普通票";
            this.rbCom.UseVisualStyleBackColor = true;
            this.rbCom.CheckedChanged += new System.EventHandler(this.rbCom_CheckedChanged);
            // 
            // lblCprice
            // 
            this.lblCprice.AutoSize = true;
            this.lblCprice.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblCprice.Location = new System.Drawing.Point(210, 136);
            this.lblCprice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCprice.Name = "lblCprice";
            this.lblCprice.Size = new System.Drawing.Size(0, 20);
            this.lblCprice.TabIndex = 7;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPrice.Location = new System.Drawing.Point(210, 115);
            this.lblPrice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(0, 20);
            this.lblPrice.TabIndex = 6;
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblTime.Location = new System.Drawing.Point(210, 94);
            this.lblTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(0, 20);
            this.lblTime.TabIndex = 5;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblType.Location = new System.Drawing.Point(210, 73);
            this.lblType.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(0, 20);
            this.lblType.TabIndex = 4;
            // 
            // lblStar
            // 
            this.lblStar.AutoSize = true;
            this.lblStar.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblStar.Location = new System.Drawing.Point(210, 53);
            this.lblStar.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStar.Name = "lblStar";
            this.lblStar.Size = new System.Drawing.Size(0, 20);
            this.lblStar.TabIndex = 3;
            // 
            // lblComplay
            // 
            this.lblComplay.AutoSize = true;
            this.lblComplay.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblComplay.Location = new System.Drawing.Point(210, 34);
            this.lblComplay.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblComplay.Name = "lblComplay";
            this.lblComplay.Size = new System.Drawing.Size(0, 20);
            this.lblComplay.TabIndex = 2;
            // 
            // pcMovieImg
            // 
            this.pcMovieImg.Location = new System.Drawing.Point(6, 21);
            this.pcMovieImg.Margin = new System.Windows.Forms.Padding(2);
            this.pcMovieImg.Name = "pcMovieImg";
            this.pcMovieImg.Size = new System.Drawing.Size(105, 148);
            this.pcMovieImg.TabIndex = 1;
            this.pcMovieImg.TabStop = false;
            // 
            // lblMovieName
            // 
            this.lblMovieName.AutoSize = true;
            this.lblMovieName.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblMovieName.Location = new System.Drawing.Point(2, 2);
            this.lblMovieName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMovieName.Name = "lblMovieName";
            this.lblMovieName.Size = new System.Drawing.Size(51, 20);
            this.lblMovieName.TabIndex = 0;
            this.lblMovieName.Text = "片名：";
            // 
            // pleft
            // 
            this.pleft.BackColor = System.Drawing.Color.Transparent;
            this.pleft.Controls.Add(this.label1);
            this.pleft.Controls.Add(this.tvMovie);
            this.pleft.Controls.Add(this.pictureBox1);
            this.pleft.Location = new System.Drawing.Point(13, 12);
            this.pleft.Margin = new System.Windows.Forms.Padding(2);
            this.pleft.Name = "pleft";
            this.pleft.Size = new System.Drawing.Size(205, 445);
            this.pleft.TabIndex = 4;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::FrmChapter12.Properties.Resources.罗小黑_眯眼;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(2, 8);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(197, 126);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pseat
            // 
            this.pseat.BackColor = System.Drawing.Color.Transparent;
            this.pseat.Location = new System.Drawing.Point(239, 208);
            this.pseat.Name = "pseat";
            this.pseat.Size = new System.Drawing.Size(581, 247);
            this.pseat.TabIndex = 5;
            // 
            // FrmMovie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(832, 469);
            this.Controls.Add(this.pseat);
            this.Controls.Add(this.pInfo);
            this.Controls.Add(this.pleft);
            this.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.ImeMode = System.Windows.Forms.ImeMode.On;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FrmMovie";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMovie_FormClosing);
            this.Load += new System.EventHandler(this.FrmMovie_Load);
            this.pInfo.ResumeLayout(false);
            this.pInfo.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcMovieImg)).EndInit();
            this.pleft.ResumeLayout(false);
            this.pleft.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TreeView tvMovie;
        private System.Windows.Forms.Panel pInfo;
        private System.Windows.Forms.Label lblCprice;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label lblStar;
        private System.Windows.Forms.Label lblComplay;
        private System.Windows.Forms.PictureBox pcMovieImg;
        private System.Windows.Forms.Label lblMovieName;
        private System.Windows.Forms.Panel pleft;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbseal;
        private System.Windows.Forms.RadioButton rbVip;
        private System.Windows.Forms.RadioButton rbCom;
        private System.Windows.Forms.Panel pseat;
        private System.Windows.Forms.Label lblDiscount;
        private System.Windows.Forms.ComboBox DisCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}

