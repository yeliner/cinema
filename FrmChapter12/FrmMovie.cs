﻿using Chapter12;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrmMovie
{
    public partial class FrmMovie : Form
    {
        public FrmMovie()
        {
            InitializeComponent();
        }
        Cinema cinema = new Cinema();
        private void FrmMovie_Load(object sender, EventArgs e)
        {
            cinema.LoadSessions();
            initTree();
            tvMovie.SelectedNode = tvMovie.Nodes[0].Nodes[0];
            initSeats();
            cinema.LoadTickes();
            

        }
        
        private void LoadSeats()
        {
            foreach (Label item in pseat.Controls)
            {
                item.BackColor = Seat.NoSoldColor;
                foreach (Ticket t in cinema.SoldTickes)
                {
                    if(t.Session==cinema.Sessions[lblTime.Text]&&t.Seat.Number==item.Text){
                        item.BackColor = Seat.SoldColor;
                        break;
                    
                    }
                }
            }
           
        }

        private void initTree()
        {
            tvMovie.Nodes.Clear();
            string movieName = "";
            TreeNode movieNode = null;
            foreach (Session item in cinema.Sessions.Values)
            {
                if (movieName != item.Movie.MovieName)
                {
                    movieNode = new TreeNode(item.Movie.MovieName);
                    tvMovie.Nodes.Add(movieNode);
                }
                TreeNode timeNode = new TreeNode(item.Time);
                movieNode.Nodes.Add(timeNode);
                movieName = item.Movie.MovieName;
            }
        }

        private void initSeats()
        {//
            int rows = 6;
            int cols = 8;
            int x = -70;
            int y = 0;
            for (int i = 1; i <= rows; i++)
            {
                for (int j = 1; j <= cols; j++)
                {
                    Label lbl= new Label();
                    lbl.Text = i + "-" + j;
                    x += 70;
                    lbl.Location = new System.Drawing.Point(x, y);
                    lbl.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
                    lbl.Size = new System.Drawing.Size(65, 25);
                    lbl.TextAlign = ContentAlignment.MiddleCenter;
                    lbl.BackColor = Seat.NoSoldColor;
                    lbl.Click += label_Click;
      
                    pseat.Controls.Add(lbl);
                }
                y += 35;
                x = -70;
            }
        }

       

        private void tvMovie_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //显示电影的详细信息
            TreeNode node = tvMovie.SelectedNode;
            if (node.Level == 1) {
                Movie mv = cinema.Sessions[node.Text].Movie;
                this.lblMovieName.Text = mv.MovieName;
                this.lblComplay.Text = mv.Company;
                this.lblStar.Text = mv.Actors;
                this.lblType.Text = mv.MovieType+"";
                this.lblTime.Text = cinema.Sessions[node.Text].Time;
                this.lblPrice.Text = mv.Price+"";
                this.lblCprice.Text = lblPrice.Text;
                this.pcMovieImg.Image = Image.FromFile(mv.Picture);
              

            }
            LoadSeats();
        }

        private void rbCom_CheckedChanged(object sender, EventArgs e)
        {
            lblCprice.Text = lblPrice.Text;
            this.DisCount.Visible = false;
            this.lblDiscount.Visible = false;
        }

        private void rbVip_CheckedChanged(object sender, EventArgs e)
        {
            lblCprice.Text = int.Parse(lblPrice.Text)*2+"";
            this.DisCount.Visible = false;
            this.lblDiscount.Visible = false;
        }
        private void rbseal_CheckedChanged(object sender, EventArgs e)
        {
            this.DisCount.SelectedItem =this.DisCount.Items[4];
            this.DisCount.Visible = true;
            this.lblDiscount.Visible = true;
            this.DisCount.TextChanged += DisCount_TextChanged;
        }

      

        private void DisCount_TextChanged(object sender, EventArgs e)
        {
            lblCprice.Text = (int.Parse(lblPrice.Text) * int.Parse(DisCount.Text) / 10) + "";
        }



        public EventHandler lbl_Click { get; set; }

        private void label_Click(object sender, EventArgs e)
        {Label lbl=((Label)sender);
            if(lbl.BackColor==Seat.NoSoldColor){
           DialogResult re= MessageBox.Show("是否够买?","提示",MessageBoxButtons.YesNo);
           if (re == DialogResult.Yes)
           {

               if (rbCom.Checked)
               {
                   Ticket ti = new Ticket();
                   ti.Seat = new Seat();
                   ti.Seat.Number = lbl.Text;
                   ti.Session = cinema.Sessions[lblTime.Text];
                   ti.Pricing();
                   ti.PrintTicket();

                   cinema.SoldTickes.Add(ti);
                   MessageBox.Show("购买成功!");
                   lbl.BackColor = Seat.SoldColor;

               }

               else if (rbVip.Checked)
               {
                   VIPTicket vi = new VIPTicket();
                   vi.Seat = new Seat();
                   vi.Seat.Number = lbl.Text;
                   vi.Session = cinema.Sessions[lblTime.Text];
                   vi.Pricing();
                   vi.PrintTicket();

                   cinema.SoldTickes.Add(vi);
                   MessageBox.Show("购买成功!");
                   lbl.BackColor = Seat.SoldColor;

               }
               else {
                   DiscountTicket di = new DiscountTicket();
                   di.Seat = new Seat();
                   di.Seat.Number = lbl.Text;
                   di.Session = cinema.Sessions[lblTime.Text];
                   di.Pricing();
                   di.PrintTicket();

                   cinema.SoldTickes.Add(di);
                   MessageBox.Show("购买成功!");
                   lbl.BackColor = Seat.SoldColor;
               }
           }
            }
        }

        private void FrmMovie_FormClosing(object sender, FormClosingEventArgs e)
        {
            cinema.SaveTickes();
        }
    }
}
