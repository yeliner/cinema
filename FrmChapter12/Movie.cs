﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chapter12
{
    [Serializable]
   public class Movie
    {
        public Movie() { }
        public Movie(string actors, string company, string movieName, MovieType movieType, string picture, int price)
        {
            this.Actors = actors;
            this.Company = company;
            this.MovieName = movieName;
            this.MovieType = movieType;
            this.Picture = picture;
            this.Price = price;
        
        }
        public string Actors { get; set; }
        public string Company { get; set; }
        public string MovieName { get; set; }
        public MovieType MovieType { get; set; }
        public string Picture { get; set; }
        public int Price { get; set; }
    }
}
