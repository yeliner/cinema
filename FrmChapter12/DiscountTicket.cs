﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Chapter12
{
    [Serializable]
    public class DiscountTicket : Ticket
    {
        public int discount { get; set; }
        public override void Pricing()
        {
            this.Price = this.Session.Movie.Price * discount/10;
        }
        public override void PrintTicket() {
            //使用文件流将电影票打印到文本文件中
            string path =  Session.Time.Replace(":", "：") + Session.Movie.MovieName + Seat.Number + ".txt";
            //  String path = @"‪F:\qyl\FrmChapter12\" + Session.Movie.MovieName + Seat.Number + ".txt";
            FileStream fs = new FileStream(path, FileMode.Create);
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine("************************************************");
            sw.WriteLine("                    打折票");
            sw.WriteLine("————————————————————————");
            sw.WriteLine("电影名：\t\t" + Session.Movie.MovieName);
            sw.WriteLine("时间：\t\t" + Session.Time);
            sw.WriteLine("座位号：\t\t" + Seat.Number);
            sw.WriteLine("座位号：\t\t" + Price);
            sw.WriteLine("************************************************");
            sw.Close();
            fs.Close();
        }
    }
}
