﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Chapter12
{
    [Serializable]
   public class Seat
    {
        public string Number { get; set; }
        public Color Color { get; set; }
        public static Color NoSoldColor { get { return Color.LightBlue; } }
        public static Color SoldColor { get { return Color.Orange; } }
     

       
    }
}
